package com.example.prathamesh.smartreminder.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.model.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;

public class SignUpActivity extends AppCompatActivity {

    //VIEW
    private EditText editTextEmail, editTextPassword,editTextFullName,editTextMobile,editTextConfirmPassword;
    private CheckBox checkBoxGender;
    private Button buttonSignUp,buttonCancel;
    private LinearLayout linearProgress;

    //Firebase Variables;
    private FirebaseAuth mAuth;
    FirebaseFirestore mFireStore;

    //Model
    UserInfoModel userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        linearProgress =(LinearLayout)findViewById(R.id.linearProgress);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextConfirmPassword = (EditText) findViewById(R.id.editTextConfirmPassword);
        buttonSignUp = (Button) findViewById(R.id.buttonSignup);
        buttonCancel = (Button)findViewById(R.id.buttonCancel);
        editTextFullName= (EditText) findViewById(R.id.editTextFullName);
        editTextMobile = (EditText) findViewById(R.id.editTextMobile);
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });


    }

    public void registerUser() {
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String fullName = editTextFullName.getText().toString();
        final String mobile = editTextMobile.getText().toString();
        String confirmPassword = editTextConfirmPassword.getText().toString();


        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (fullName.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextFullName;
            editTextFullName.setError("full name is a required field");
        }
        if (password.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("password is a required field");
        }
        if (editTextConfirmPassword.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextConfirmPassword;
            editTextConfirmPassword.setError("password is a required field");
        }
        if (password.length() < 6) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("password must be greater than 6 letters");
        }
        if(!password.matches(confirmPassword))
        {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("password does not matches");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a required field");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. must be of 10 digit");
        }
        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is not valid");
        }

        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            linearProgress.setVisibility(View.VISIBLE);

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                linearProgress.setVisibility(View.GONE);
                                Toast.makeText(SignUpActivity.this, "Authentication Successfull.", Toast.LENGTH_SHORT).show();
                                String userId = mAuth.getUid();
                                userInfo = new UserInfoModel(fullName,email,password,mobile,userId);
                                mFireStore.collection("UserInfo").document().set(userInfo)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(SignUpActivity.this, "data added", Toast.LENGTH_LONG).show();
                                                    startActivity(new Intent(SignUpActivity.this, HomeActivity.class));
                                                } else {
                                                    Toast.makeText(SignUpActivity.this, "data not added", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });

                            } else {
                                linearProgress.setVisibility(View.GONE);
                                if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                    Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });





        }
    }
}

