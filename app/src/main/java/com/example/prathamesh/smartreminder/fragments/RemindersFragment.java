package com.example.prathamesh.smartreminder.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.activities.BirthdayReminderActivity;
import com.example.prathamesh.smartreminder.activities.GeneralReminderActivity;
import com.example.prathamesh.smartreminder.activities.HealthReminderActivity;
import com.example.prathamesh.smartreminder.activities.HomeActivity;
import com.example.prathamesh.smartreminder.activities.MapsActivity;
import com.example.prathamesh.smartreminder.activities.SetAlarmsActivity;
import com.example.prathamesh.smartreminder.model.BirthdayReminderModel;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.model.HealthReminderModel;
import com.example.prathamesh.smartreminder.services.MyBirthdayAlarm;
import com.example.prathamesh.smartreminder.services.MyGeneralAlarm;
import com.example.prathamesh.smartreminder.services.MyHealthAlarm;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;


public class RemindersFragment extends Fragment {

    //VIEWS
    LinearLayout generalReminder,birthdayReminder,healthReminder,locationReminder;
    View view;
    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String userId;
    ArrayList<GeneralReminderModel> generalReminderList ;
    ArrayList<BirthdayReminderModel> birthdayReminderList;
    ArrayList<HealthReminderModel> healthReminderList;
    ArrayList<String> healthTimeList;
    public  static  String XXX;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        userId = user.getUid();
        MyGeneralAlarm.context = getContext();
        MyBirthdayAlarm.context = getContext();
        MyHealthAlarm.context = getContext();
        mFirestore=FirebaseFirestore.getInstance();
        if(HomeActivity.OTHER_FLAG!=null) {

            userId= HomeActivity.USER_ID;
        }
        else
        { if(user!=null)
            userId = user.getUid();
        }

        getGeneralAlarms(userId);
        getBirthdayAlarms(userId);
        getHealthAlarms(userId);

        mFirestore = FirebaseFirestore.getInstance();
        view = inflater.inflate(R.layout.fragment_reminders, container, false);
        generalReminder = (LinearLayout)view.findViewById(R.id.generalReminder);
        healthReminder = (LinearLayout)view.findViewById(R.id.healthReminder);
        birthdayReminder = (LinearLayout)view.findViewById(R.id.birthdayReminder);
        locationReminder = (LinearLayout)view.findViewById(R.id.locationReminder);


        generalReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GeneralReminderActivity.class);
                startActivity(intent);
            }
        });
        healthReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HealthReminderActivity.class);
                startActivity(intent);
            }
        });

        birthdayReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BirthdayReminderActivity.class);
                startActivity(intent);
            }
        });
        locationReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
            }
        });



        return  view;
    }

    private void getBirthdayAlarms(final String userId) {

        mFirestore.collection(BirthdayReminderModel.FIRESTORE_COLLECTION_BIRTHDAY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful()) {
                            birthdayReminderList = new ArrayList<>();
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                                if (userId.matches(documentSnapshot.getData().get("userId").toString())) {
                                    BirthdayReminderModel birthdayReminderModel = documentSnapshot.toObject(BirthdayReminderModel.class);
                                    birthdayReminderList.add(birthdayReminderModel);
                                }

                            }

                            if (birthdayReminderList != null) {
                                Log.d("Size ",birthdayReminderList.size()+"");
                                //SetAlarmsActivity.generalReminderList = generalReminderList;
                                //startActivity(new Intent(getActivity(), SetAlarmsActivity.class));


                                for(int i=0; i< birthdayReminderList.size();i++)
                                {
                                    String date =  birthdayReminderList.get(i).getDate();
                                    String time =birthdayReminderList.get(i).getTime();
                                    String[] val = date.split("/");
                                    int day = Integer.parseInt(val[0]);
                                    int month = Integer.parseInt(val[1]);
                                    int year = Integer.parseInt(val[2]);
                                    Log.d("Year",year+"");
                                    Log.d("Month",month+"");
                                    Log.d("Day",day+"");
                                    String[] valtime = time.split(":");
                                    Log.d("Time1",time+"");
                                    int hour = Integer.parseInt(valtime[0]);
                                    String[] valmin = valtime[1].split(" ");
                                    int min = Integer.parseInt( valmin[0]);

                                    Calendar calendar = Calendar.getInstance();
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        calendar.set(
                                                year,
                                                month-1,
                                                day,
                                                hour,
                                                min,
                                                0);

                                        Log.d("Mini", String.valueOf(min));
                                        Log.d("year", String.valueOf(year));
                                        Log.d("mon", String.valueOf(month));
                                        Log.d("day", String.valueOf(day));

                                    } else {
                                        calendar.set(
                                                year,
                                                month-1,
                                                day,
                                                hour,
                                                min,
                                                0);
                                    }
                                    Log.d("Mili", String.valueOf(calendar.getTimeInMillis()));
                                    setBirthdayAlarm(calendar.getTimeInMillis());



                                }
                            }

                        }

                    }
                });
    }



    public void getGeneralAlarms(final String userId)
    {
Log.d("UserId",userId);
        mFirestore.collection(GeneralReminderModel.FIRESTORE_COLLECTION_GENERAL)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                   if(task.isSuccessful()) {

                       generalReminderList = new ArrayList<>();
                       for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                           if(userId.matches(documentSnapshot.getData().get("userId").toString()))
                           {
                               GeneralReminderModel generalReminderModel = documentSnapshot.toObject(GeneralReminderModel.class);
                               generalReminderList.add(generalReminderModel);
                           }
                       }
                       if (generalReminderList != null) {
                           Log.d("Size ",generalReminderList.size()+"");
                           SetAlarmsActivity.generalReminderList = generalReminderList;
                           //startActivity(new Intent(getActivity(), SetAlarmsActivity.class));


                           for(int i=0; i< generalReminderList.size();i++)
                           {
                               String date =  generalReminderList.get(i).getDate();
                               String time =generalReminderList.get(i).getTime();
                               String[] val = date.split("/");
                               int day = Integer.parseInt(val[0]);
                               int month = Integer.parseInt(val[1]);
                               int year = Integer.parseInt(val[2]);
                               Log.d("Year",year+"");
                               Log.d("Month",month+"");
                               Log.d("Day",day+"");
                               String[] valtime = time.split(":");
                               Log.d("Time1",time+"");
                               int hour = Integer.parseInt(valtime[0]);
                               String[] valmin = valtime[1].split(" ");
                               int min = Integer.parseInt( valmin[0]);

                               Calendar calendar = Calendar.getInstance();
                               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                   calendar.set(
                                           year,
                                           month-1,
                                           day,
                                           hour,
                                           min,
                                           0);

                                   Log.d("Mini", String.valueOf(min));
                                   Log.d("year", String.valueOf(year));
                                   Log.d("mon", String.valueOf(month));
                                   Log.d("day", String.valueOf(day));

                               } else {
                                   calendar.set(
                                           year,
                                           month-1,
                                           day,
                                           hour,
                                           min,
                                           0);
                               }
                               Log.d("Mili", String.valueOf(calendar.getTimeInMillis()));
                               setGeneralAlarm(calendar.getTimeInMillis());



                           }
                       }
                   }

                    }


                });
    }

    public void getHealthAlarms(final String userId)
    {
        mFirestore.collection(HealthReminderModel.FIRESTORE_COLLECTION_HEALTH)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        healthReminderList = new ArrayList<>();
                        for(QueryDocumentSnapshot documentSnapshot:task.getResult())
                        {
                            if(userId.matches(documentSnapshot.getData().get("userId").toString()))
                            {
                                HealthReminderModel healthReminderModel = documentSnapshot.toObject(HealthReminderModel.class);
                                healthReminderList.add(healthReminderModel);
                            }

                        }
                        if (healthReminderList != null) {
                            Log.d("HSize ", healthReminderList.size() + "");


                            for(int i = 0; i<healthReminderList.size();i++)
                            {
                                String date = healthReminderList.get(i).getDate();
                                String[] val = date.split("/");
                                int day = Integer.parseInt(val[0]);
                                int month = Integer.parseInt(val[1]);
                                int year = Integer.parseInt(val[2]);
                                healthTimeList = new ArrayList<>();
                                for(int j =0 ;j<healthReminderList.get(i).getHealthtime().size();j++) {
                                    String time1  = healthReminderList.get(i).getHealthtime().get(j);
                                    healthTimeList.add(time1);

                                    String time = healthTimeList.get(j);
                                    String[] valtime = time.split(":");
                                    Log.d("Time1",time+"");
                                    int hour = Integer.parseInt(valtime[0]);
                                    String[] valmin = valtime[1].split(" ");
                                    int min = Integer.parseInt( valmin[0]);

                                    Calendar calendar = Calendar.getInstance();
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        calendar.set(
                                                year,
                                                month-1,
                                                day,
                                                hour,
                                                min,
                                                0);

                                        Log.d("Mini", String.valueOf(min));
                                        Log.d("year", String.valueOf(year));
                                        Log.d("mon", String.valueOf(month));
                                        Log.d("day", String.valueOf(day));

                                    } else {
                                        calendar.set(
                                                year,
                                                month-1,
                                                day,
                                                hour,
                                                min,
                                                0);
                                    }
                                    Log.d("Mili", String.valueOf(calendar.getTimeInMillis()));
                                    setHealthAlarm(calendar.getTimeInMillis());



                                }



                            }
                        }
                    }
                });
    }

    public  void setGeneralAlarm(long timeInMillis) {

        AlarmManager alarmManager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(getContext(), MyGeneralAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
       // Toast.makeText(getActivity(), "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);


    }
    private void setBirthdayAlarm(long timeInMillis) {


        AlarmManager alarmManager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(getContext(), MyBirthdayAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
       // Toast.makeText(getActivity(), "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);
    }

    private void setHealthAlarm(long timeInMillis)
    {
        AlarmManager alarmManager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(getContext(), MyHealthAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
       // Toast.makeText(getActivity(), "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);
    }


}
