package com.example.prathamesh.smartreminder.model;

public class BirthdayReminderModel {
    public final static String FIRESTORE_COLLECTION_BIRTHDAY = "birthdayReminders";
    String name,description,date,time,userId;

    public BirthdayReminderModel() {
    }

    public BirthdayReminderModel(String name, String description, String date, String time, String userId) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.time = time;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
