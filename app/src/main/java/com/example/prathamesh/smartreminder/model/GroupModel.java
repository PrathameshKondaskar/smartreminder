package com.example.prathamesh.smartreminder.model;

public class GroupModel {

    String username,groupName;

    public GroupModel(String username, String groupName) {
        this.username = username;
        this.groupName = groupName;
    }

    public GroupModel() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
