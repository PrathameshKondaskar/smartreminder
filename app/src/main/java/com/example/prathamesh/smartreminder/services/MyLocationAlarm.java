package com.example.prathamesh.smartreminder.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.activities.HomeActivity;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.model.LocationReminederModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MyLocationAlarm extends BroadcastReceiver {


    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

    public static Context context;
    ArrayList<LocationReminederModel> locationReminederModelList;

    String userId;
    String notificationName = "", notificationDescription = "";
    String date, time;
    String NOTIFICATION_CHANNEL_ID = "My Notification";
    MediaPlayer mediaPlayer;


    ArrayList<String> nameList = new ArrayList<>();
    ArrayList<String> descList = new ArrayList<>();
    ArrayList<String> dateList = new ArrayList<>();
    ArrayList<String> timeList = new ArrayList<>();


    @Override
    public void onReceive(Context context, Intent intent) {

        Date currentdate = Calendar.getInstance().getTime();  // to get the date
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy"); // getting date in this format
        //date = df.format(currentdate.getTime());

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        date = mDay + "/" + mMonth + "/" + mYear;

        Log.d("Date", date);
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH");
        String time1 = dateFormat.format(currentTime);

        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);

        if (Integer.parseInt(time1) > 12) {
            hour += 12;
        }
        String AM_PM;
        if (hour < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        time = hour + ":" + minute + " " + AM_PM;


        Log.d("Time2", time);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        if(user!=null) {
            userId = user.getUid();


            getLocationalarmData();
        }

        mediaPlayer = MediaPlayer.create(context, R.raw.ringtone);}




    public void getLocationalarmData() {
        mFirestore.collection(LocationReminederModel.FIRESTORE_COLLECTION_LOCTATION)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            Log.d("TAG", task.getResult().isEmpty() + " => ");
                            locationReminederModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if (userId.matches((String) document.getData().get("userId"))) {
                                    LocationReminederModel locationReminederModel = document.toObject(LocationReminederModel.class);
                                    locationReminederModelList.add(locationReminederModel);
                                    //Toast.makeText(context, " data location retrieved", Toast.LENGTH_SHORT).show();
                                }

                            }

                            for (int i = 0; i < locationReminederModelList.size(); i++) {
                                String name = locationReminederModelList.get(i).getName();
                                String desc = locationReminederModelList.get(i).getDescription();
                                String tarikh = locationReminederModelList.get(i).getDate();
                                String vel = locationReminederModelList.get(i).getTime();

                                nameList.add(name);
                                descList.add(desc);
                                dateList.add(tarikh);
                                timeList.add(vel);
//                                Log.d("Name", i + " " + name);
//                                Log.d("DATE", i + " " + tarikh);
//                                Log.d("TIME", i + " " + vel);

                            }
//                                Intent intent = new Intent(GeneralReminderActivity.this,MyAlarm.class);
//                                intent.putExtra("Date",tarikhlist);
//                                intent.putExtra("Time",vellist);
//                                intent.putExtra("Name",name);


                            Log.d("SIZE", String.valueOf(dateList.size()));
//                            for (int j = 0; j < dateList.size(); j++) {
//                                Log.d("j", String.valueOf(dateList.get(j)));
//                                if (dateList.get(j).matches(date)) {
//                                    Log.d("DateList", dateList.get(j));
//                                    Log.d("Dates", date);
                            for (int j = 0; j < timeList.size(); j++) {

                                if (timeList.get(j).matches(time)) {
                                    notificationName = nameList.get(j);
                                    Log.d("NAME", notificationName);
                                    notificationDescription = descList.get(j);
                                    Log.d("NAME1", notificationDescription);
                                    Log.d("TAG", "hade ye");
                                    addNotification();

                                }
                            }
                            //}
                            // }
                        } else {

                        }
                    }
                });


    }


    private void addNotification() {
        Log.d("Notifyy", "vaj Location bala vaj");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.splash1)
                        .setContentTitle(notificationName)
                        .setContentText(notificationDescription);
        Intent notificationIntent = new Intent(context, HomeActivity.class);
        notificationIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        // Add as notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel nChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
            nChannel.enableLights(true);
            assert manager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            manager.createNotificationChannel(nChannel);
        }
        assert manager != null;
        manager.notify(0, builder.build());
        Log.d("Notify", "vaj bala vaj");
        mediaPlayer.start();

    }
}
