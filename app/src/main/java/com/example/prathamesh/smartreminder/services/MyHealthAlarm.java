package com.example.prathamesh.smartreminder.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.activities.HomeActivity;
import com.example.prathamesh.smartreminder.model.BirthdayReminderModel;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.model.HealthReminderModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static android.content.Context.NOTIFICATION_SERVICE;

public class MyHealthAlarm extends BroadcastReceiver {


    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

    public static Context context;
    ArrayList<HealthReminderModel> healthReminderModelList;
    String userId;
    String notificationName = "", notificationDescription = "";
    String date, time;
    String NOTIFICATION_CHANNEL_ID = "My Notification";
    MediaPlayer mediaPlayer;

    ArrayList<String> dateList = new ArrayList<>();
    ArrayList<String> timeList = new ArrayList<>();
    ArrayList<String> nameList = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {

        final Calendar c = Calendar.getInstance();
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH");
        String time1 = dateFormat.format(currentTime);

        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);

        if (Integer.parseInt(time1) > 12) {
            hour += 12;
        }
        String AM_PM;
        if (hour < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        time = hour + ":" + minute + " " + AM_PM;


        Log.d("Time", time);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        if(user!=null) {
        userId = user.getUid();


            getDataFromFireBase();
        }
        mediaPlayer = MediaPlayer.create(context, R.raw.ringtone);
    }

    public void getDataFromFireBase() {

        mFirestore.collection(HealthReminderModel.FIRESTORE_COLLECTION_HEALTH)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            Log.d("TAG", task.getResult().isEmpty() + " <> ");
                            healthReminderModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HERE", "here");
                                if (userId.matches((String) document.getData().get("userId"))) {
                                HealthReminderModel healthReminderModel = document.toObject(HealthReminderModel.class);
                                healthReminderModelList.add(healthReminderModel);
                                Log.d("hlist", String.valueOf(healthReminderModelList.size()));
                                //Toast.makeText(context, " data health retrieved", Toast.LENGTH_SHORT).show();
                                }

                            }
                            Log.d("hlists", String.valueOf(healthReminderModelList.size()));
                            for (int i = 0; i < healthReminderModelList.size(); i++) {
                                String name = healthReminderModelList.get(i).getName();
                                String tarikh = healthReminderModelList.get(i).getDate();
                                String vel="";
                                for(int j=0;j<healthReminderModelList.get(i).getHealthtime().size();j++) {
                                     vel= healthReminderModelList.get(i).getHealthtime().get(j);
                                    timeList.add(vel);
                                }
                                nameList.add(name);
                                dateList.add(tarikh);

                                Log.d("Name", i + " " + name);
                                Log.d("DATE", i + " " + tarikh);
                                Log.d("TIME", i + " " + vel);

                            }

                            Log.d("SIZES", String.valueOf(timeList.size()));
                            for (int j = 0; j < timeList.size(); j++) {

                                if (timeList.get(j).matches(time)) {
                                   // notificationName = nameList.get(j);
                                    //Log.d("NAME", notificationName);
                                    Log.d("TAG", "hade ye");
                                    addNotification();

                                }
                            }

                        } else {

                         Log.d("ERR","err");
                        }
                    }
                });
    }


    private void addNotification() {
        Log.d("Notifyy", "vaj bala vaj");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.splash1)
                        .setContentTitle("Medicine Time")
                        .setContentText("Take your medicine on time");
        Intent notificationIntent = new Intent(context, HomeActivity.class);
        notificationIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        // Add as notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel nChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
            nChannel.enableLights(true);
            assert manager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            manager.createNotificationChannel(nChannel);
        }
        assert manager != null;
        manager.notify(new Random().nextInt(), builder.build());
        Log.d("Notify","vaj bala vaj");
        mediaPlayer.start();



    }
}
