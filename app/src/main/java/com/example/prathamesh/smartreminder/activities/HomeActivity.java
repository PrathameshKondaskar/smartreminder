package com.example.prathamesh.smartreminder.activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.fragments.RemindersFragment;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.model.LocationReminederModel;
import com.example.prathamesh.smartreminder.services.MyGeneralAlarm;
import com.example.prathamesh.smartreminder.services.MyLocationAlarm;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    FirebaseFirestore db ;
    public static  String OTHER_FLAG,USER_ID;

    double currentLatitude,currentLongitude;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;


    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String userId;
    ArrayList<LocationReminederModel> locationReminderList;
    String NOTIFICATION_CHANNEL_ID = "My Notification";
    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mediaPlayer= MediaPlayer.create(HomeActivity.this, R.raw.ringtone);
        mFirestore = FirebaseFirestore.getInstance();

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if(HomeActivity.OTHER_FLAG!=null) {

            userId= HomeActivity.USER_ID;
        }
        else
        {
            if(user!=null)
            userId = user.getUid();
        }

        if(OTHER_FLAG!=null && USER_ID!=null)
        {
            //Toast.makeText(this, "Apna time Aayega"+USER_ID, Toast.LENGTH_LONG).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        getDisplay(R.id.nav_reminders);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(HomeActivity.this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void getDisplay(int id) {
        Fragment fragment = null;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction().addToBackStack("null");
        switch (id) {

            case R.id.nav_reminders:
                fragment = new RemindersFragment();
                break;
            case R.id.nav_myFamily:
                startActivity(new Intent(this,MyFamilyActivity.class));

                break;
            case R.id.nav_emergencyContact:
                startActivity(new Intent(this,EmergencyContactActivity.class));

                break;
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(this,LoginActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);


                break;
            case R.id.reminderHistory:
                startActivity(new Intent(this,ReminderHistoryActivity.class));

                break;
            case R.id.nav_showLocations:
                startActivity(new Intent(this,ShowLocationsActivity.class));

                break;
        }
        if (fragment != null) {

            ft.replace(R.id.content_home, fragment);
            ft.commit();
        }


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        getDisplay(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location == null) {
            Toast.makeText(this, "cant get current location", Toast.LENGTH_LONG).show();
        }else {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

            getDataFromFirebase(userId);
      //  Log.d("LAT", String.valueOf(currentLatitude));
       // Toast.makeText(this, "lat: "+currentLatitude+" long: "+currentLongitude, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
       // Toast.makeText(this, "HERE", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,HomeActivity.this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    public void getDataFromFirebase(final String userId)
    {

        mFirestore.collection(LocationReminederModel.FIRESTORE_COLLECTION_LOCTATION)
                .whereEqualTo("status",true)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()) {

                            locationReminderList = new ArrayList<>();
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                                if(userId.matches(documentSnapshot.getData().get("userId").toString()))
                                {

                                    LocationReminederModel locationReminederModel = documentSnapshot.toObject(LocationReminederModel.class);
                                    locationReminederModel.setDocId(documentSnapshot.getId());
                                    locationReminderList.add(locationReminederModel);

                                }

                            }
                            if (locationReminderList != null) {
                                ShowLocationsActivity.locationReminederModelList = locationReminderList;
                                Log.d("Size ",locationReminderList.size()+"");


                                for(int i=0; i< locationReminderList.size();i++)
                                {
                                    String date =  locationReminderList.get(i).getDate();
                                    String time =locationReminderList.get(i).getTime();
                                    String[] val = date.split("/");
                                    int day = Integer.parseInt(val[0]);
                                    int month = Integer.parseInt(val[1]);
                                    int year = Integer.parseInt(val[2]);
                                    Log.d("Year",year+"");
                                    Log.d("Month",month+"");
                                    Log.d("Day",day+"");
                                    String[] valtime = time.split(":");
                                    Log.d("Time1",time+"");
                                    int hour = Integer.parseInt(valtime[0]);
                                    String[] valmin = valtime[1].split(" ");
                                    int min = Integer.parseInt( valmin[0]);

                                    Calendar calendar = Calendar.getInstance();
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        calendar.set(
                                                year,
                                                month-1,
                                                day,
                                                hour,
                                                min,
                                                0);

                                        Log.d("Mini", String.valueOf(min));
                                        Log.d("year", String.valueOf(year));
                                        Log.d("mon", String.valueOf(month));
                                        Log.d("day", String.valueOf(day));

                                    } else {
                                        calendar.set(
                                                year,
                                                month-1,
                                                day,
                                                hour,
                                                min,
                                                0);
                                    }
                                    Log.d("Mili", String.valueOf(calendar.getTimeInMillis()));
                                    setLocationAlarm(calendar.getTimeInMillis());



                                }
                                for(int i =0;i<locationReminderList.size();i++)
                                {
                                    double latitude = locationReminderList.get(i).getLatitude();
                                    double longitude = locationReminderList.get(i).getLongitude();
                                    String name = locationReminderList.get(i).getName();
                                    String notification = locationReminderList.get(i).getDescription();
//
//                                    double theta = currentLongitude - longitude;
//                                    double dist = Math.sin(deg2rad(lat1))
//                                            * Math.sin(deg2rad(lat2))
//                                            + Math.cos(deg2rad(lat1))
//                                            * Math.cos(deg2rad(lat2))
//                                            * Math.cos(deg2rad(theta));
//                                    dist = Math.acos(dist);
//                                    dist = rad2deg(dist);
//                                    dist = dist * 60 * 1.1515;


                                    Location startPoint=new Location("locationA");
                                    startPoint.setLatitude(currentLatitude);
                                    startPoint.setLongitude(currentLongitude);

                                    Location endPoint=new Location("locationA");
                                    endPoint.setLatitude(latitude);
                                    endPoint.setLongitude(longitude);

                                    double distance =(startPoint.distanceTo(endPoint))/1000;
                                    Log.d("Distance",distance+"");

                            if(distance < 2) {
                                HashMap<String ,Object> map = new HashMap<>();
                                map.put("status",false);
                                mFirestore.collection(LocationReminederModel.FIRESTORE_COLLECTION_LOCTATION)
                                        .document(locationReminderList.get(i).getDocId()).update(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });

                                Log.d("Notifyy", "vaj bala vaj");
                                NotificationCompat.Builder builder =
                                        new NotificationCompat.Builder(HomeActivity.this)
                                                .setSmallIcon(R.drawable.splash1)
                                                .setContentTitle(name)
                                                .setContentText(notification);
                                Intent notificationIntent = new Intent(HomeActivity.this, HomeActivity.class);
                                notificationIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
                                PendingIntent contentIntent = PendingIntent.getActivity(HomeActivity.this, 0, notificationIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);
                                builder.setContentIntent(contentIntent);
                                // Add as notification
                                NotificationManager manager = (NotificationManager) HomeActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    NotificationChannel nChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
                                    nChannel.enableLights(true);
                                    assert manager != null;
                                    builder.setChannelId(NOTIFICATION_CHANNEL_ID);
                                    manager.createNotificationChannel(nChannel);
                                }
                                assert manager != null;
                                manager.notify(0, builder.build());
                                Log.d("Notify", "vaj bala vaj");
                                mediaPlayer.start();


                            }




                                }
                            }
                        }

                    }


                });

    }


    public  void setLocationAlarm(long timeInMillis) {

        AlarmManager alarmManager = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(this, MyLocationAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
       // Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);


    }
}
