package com.example.prathamesh.smartreminder.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.model.EmergencyContactModel;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.model.HealthReminderModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class EmergencyContactActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks
        ,GoogleApiClient.OnConnectionFailedListener {

    FloatingActionButton fab;
    ListView listViewContact;
    static final int PICK_CONTACT = 1;
    public static String HELP_ME ="";
    String helpMe="";



    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String userId;

    //Variables
    String number, name;
    int count = 0;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    double currentLatitude, currentLongitude;
    Button buttonLocatioin;
    LocationManager locationManager;
    Location location;
    ProgressBar progressBar;


    ArrayList<EmergencyContactModel> emergencyContactModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        userId = user.getUid();

        mFirestore = FirebaseFirestore.getInstance();
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        Criteria criteria = new Criteria();
//
//        String provider = locationManager.getBestProvider(criteria, true);
//        location = locationManager.getLastKnownLocation(provider);
//        onLocationChanged(location);
        listViewContact = (ListView)findViewById(R.id.listViewContact);
        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(emergencyContactModelList.size()<3) {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);
                }
                else {
                    Toast.makeText(EmergencyContactActivity.this, "Only 3 Emergency Contact can be added", Toast.LENGTH_SHORT).show();
                }

            }
        });

        Intent intent = getIntent();
        helpMe= intent.getStringExtra(HELP_ME);

//
//        if(count>0)
//        {
//            sendDataToFirebase();
//        }
        getDataFromFireBase();

      listViewContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
              Intent callIntent = new Intent(Intent.ACTION_DIAL);
              callIntent.setData(Uri.parse("tel:" +emergencyContactModelList.get(i).getNumber()));
              startActivity(callIntent);

          }
      });

//      buttonLocatioin.setOnClickListener(new View.OnClickListener() {
//          @Override
//          public void onClick(View view) {
//
//              //Location currentLocation = new Location(LocationManager.GPS_PROVIDER);
//
//
//              String uri ="Help me I am in trouble at "+ "http://maps.google.com/?q=loc:" +currentLatitude+","+currentLongitude;
//
//              SmsManager smsManager = SmsManager.getDefault();
//              StringBuffer smsBody = new StringBuffer();
//              smsBody.append(Uri.parse(uri));
//              smsManager.sendTextMessage("9833741601", null, smsBody.toString(), null, null);
//          }
//      });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(EmergencyContactActivity.this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT) :
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c =  managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {


                        String id =c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
                                    null, null);
                            phones.moveToFirst();
                      number = phones.getString(phones.getColumnIndex("data1"));
                            System.out.println("number is:"+number);
                        }
                      name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        System.out.println("name is:"+name);
                    }
                    if(name!=null) {
                        sendDataToFirebase();
                        getDataFromFireBase();
                    }

                }
                break;
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void sendDataToFirebase()
    {

        EmergencyContactModel emergencyContactModel = new EmergencyContactModel(name,number,userId);
        mFirestore.collection(EmergencyContactModel.FIRESTORE_COLLECTION_EMERGENCY_CONTACT)
                .document().set(emergencyContactModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                   // Toast.makeText(EmergencyContactActivity.this, "data added", Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(EmergencyContactActivity.this, "data not added", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void getDataFromFireBase() {
        progressBar.setVisibility(View.VISIBLE);
        mFirestore.collection(EmergencyContactModel.FIRESTORE_COLLECTION_EMERGENCY_CONTACT)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            Log.d("TAG", task.getResult().isEmpty() + " <> ");
                            emergencyContactModelList = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HERE", "here");
                                if (userId.matches((String) document.getData().get("userId"))) {
                                    EmergencyContactModel emergencyContactModel = document.toObject(EmergencyContactModel.class);
                                    emergencyContactModelList.add(emergencyContactModel);
                                    Log.d("hlist", String.valueOf(emergencyContactModelList.size()));
                                  //  Toast.makeText(EmergencyContactActivity.this, "Contact Added", Toast.LENGTH_SHORT).show();
                                }
                            }
//                            String uri ="Help me I am in trouble at "+ "http://maps.google.com/?q=loc:" +currentLatitude+","+currentLongitude;
//
//                            SmsManager smsManager = SmsManager.getDefault();
//                            StringBuffer smsBody = new StringBuffer();
//                            smsBody.append(Uri.parse(uri));
//                            for(int i =0;i<emergencyContactModelList.size();i++) {
//                                smsManager.sendTextMessage(emergencyContactModelList.get(i).getNumber(), null, smsBody.toString(), null, null);
//                            }

                            if(helpMe!= null)
                            {
                                String uri ="Help me I am in trouble at "+ "http://maps.google.com/?q=loc:" +currentLatitude+","+currentLongitude;

                                SmsManager smsManager = SmsManager.getDefault();
                                StringBuffer smsBody = new StringBuffer();
                                smsBody.append(Uri.parse(uri));
                                for(int i =0;i<emergencyContactModelList.size();i++) {
                                    Toast.makeText(EmergencyContactActivity.this, "message sent", Toast.LENGTH_SHORT).show();
                                    smsManager.sendTextMessage(emergencyContactModelList.get(i).getNumber(), null, smsBody.toString(), null, null);
                                }
                            }
                            CustomAdapter customAdapter = new CustomAdapter(EmergencyContactActivity.this,android.R.layout.simple_list_item_1,emergencyContactModelList);
                            listViewContact.setAdapter(customAdapter);


                        } else {

                            Log.d("ERR","err");
                        }
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
       // Toast.makeText(this, "HERE", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,EmergencyContactActivity.this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(this, "cant get current location", Toast.LENGTH_LONG).show();
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

        }
    }


    public class CustomAdapter extends ArrayAdapter<EmergencyContactModel>
    {
        List<EmergencyContactModel> contactModelList;

        public CustomAdapter(Context context, int resource, List<EmergencyContactModel> contactModelList) {
            super(context, resource, contactModelList);
            this.contactModelList = contactModelList;
        }


        @Override
        public View getView(int position,View view, ViewGroup parent) {
            TextView textViewName,textViewNumber;
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.custom_emergency_contact, parent, false);

            textViewName = (TextView)view.findViewById(R.id.textViewName);
            textViewNumber= (TextView)view.findViewById(R.id.textViewNumber);

            textViewName.setText(contactModelList.get(position).getName());
            textViewNumber.setText(contactModelList.get(position).getNumber());

            return  view;
        }
    }
}
