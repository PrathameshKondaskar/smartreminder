package com.example.prathamesh.smartreminder.model;

import java.util.List;

public class CommonReminderModel {
    public final static String FIRESTORE_COLLECTION_COMMON = "commonReminders";
    String name;
     List<String> time;
    String userId;

    public CommonReminderModel() {
    }

    public CommonReminderModel(String name, List<String> time, String userId) {
        this.name = name;
        this.time = time;
        this.userId = userId;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
