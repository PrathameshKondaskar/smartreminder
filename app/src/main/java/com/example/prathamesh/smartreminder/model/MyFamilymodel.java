package com.example.prathamesh.smartreminder.model;

public class MyFamilymodel {
    public final static String FIRESTORE_COLLECTION_MYFAMILY = "myFamily";
    String name ,number,flag;
    String userId;

    public MyFamilymodel(String name, String number, String flag, String userId) {
        this.name = name;
        this.number = number;
        this.flag = flag;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public MyFamilymodel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
