package com.example.prathamesh.smartreminder.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.model.CommonReminderModel;
import com.example.prathamesh.smartreminder.model.EmergencyContactModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class ReminderHistoryActivity extends AppCompatActivity {


    ListView listViewRemiderHistory;
    ProgressBar progressBar;

    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String userId;

    ArrayList<CommonReminderModel> commonReminderModellist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listViewRemiderHistory= (ListView)findViewById(R.id.listViewReminderHistory);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        userId = user.getUid();
        mFirestore = FirebaseFirestore.getInstance();

        getDataFromFirebase();
        //getHistory();

    }

    public  void getDataFromFirebase()
    {
        mFirestore.collection(CommonReminderModel.FIRESTORE_COLLECTION_COMMON)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful())
                        {
                            progressBar.setVisibility(View.GONE);
                            commonReminderModellist = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if(userId.matches(document.getData().get("userId").toString()))
                                {
                                       CommonReminderModel commonReminderModel = document.toObject(CommonReminderModel.class);
                                       commonReminderModellist.add(commonReminderModel);
                                }
                            }
                            CustomAdapter customAdapter = new CustomAdapter(ReminderHistoryActivity.this,android.R.layout.simple_list_item_1,commonReminderModellist);
                            listViewRemiderHistory.setAdapter(customAdapter);
                        }
                        else
                        {

                        }
                    }
                });
    }
    public  void getHistory()
    {
        mFirestore.collection(CommonReminderModel.FIRESTORE_COLLECTION_COMMON)
                .whereEqualTo("userId0", userId)
                .orderBy("time", Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {


                        for (QueryDocumentSnapshot document : queryDocumentSnapshots)
                        {
                            CommonReminderModel commonReminderModel = document.toObject(CommonReminderModel.class);
                            commonReminderModellist.add(commonReminderModel);
                        }
                        CustomAdapter customAdapter = new CustomAdapter(ReminderHistoryActivity.this,android.R.layout.simple_list_item_1,commonReminderModellist);
                        listViewRemiderHistory.setAdapter(customAdapter);
                    }
                });
    }

    public class CustomAdapter extends ArrayAdapter<CommonReminderModel>
    {
        List<CommonReminderModel> commonReminderModelList;

        public CustomAdapter(Context context, int resource, List<CommonReminderModel> commonReminderModelList) {
            super(context, resource, commonReminderModelList);
            this.commonReminderModelList = commonReminderModelList;
        }


        @Override
        public View getView(int position, View view, ViewGroup parent) {
            TextView textViewName,textViewTime;
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.custom_history, parent, false);

            textViewName = (TextView)view.findViewById(R.id.textViewName);
            textViewTime= (TextView)view.findViewById(R.id.textViewTime);

            textViewName.setText(commonReminderModelList.get(position).getName());

            ArrayList<String> items = new ArrayList<>();
            for(int i= 0;i<commonReminderModelList.size();i++){
                String s="";

                for(int j=0;j<commonReminderModelList.get(i).getTime().size();j++) {
                    if(s.matches(""))
                        s = s+commonReminderModelList.get(i).getTime().get(j);
                    else
                        s = s+", "+commonReminderModelList.get(i).getTime().get(j);
                    Log.d("alarm", commonReminderModelList.get(i).getTime().get(j));
                    Log.d("Size", String.valueOf(commonReminderModelList.get(i).getTime().size()));
                }
                items.add(s);


            }
           // String result = TextUtils.join(", ", items.get(position).split(","));
            textViewTime.setText(items.get(position));

            Log.d("Item", String.valueOf(items.size()));


            return  view;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
