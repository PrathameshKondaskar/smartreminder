package com.example.prathamesh.smartreminder.services;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.activities.EmergencyContactActivity;

public class ExampleWidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for(int appWidgetId :appWidgetIds)
        {
            Intent intent = new Intent(context, EmergencyContactActivity.class);
            intent.putExtra(EmergencyContactActivity.HELP_ME,"helpMe");
            PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,0);

            RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.widget);
            views.setOnClickPendingIntent(R.id.button,pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId,views);

        }
    }
}
