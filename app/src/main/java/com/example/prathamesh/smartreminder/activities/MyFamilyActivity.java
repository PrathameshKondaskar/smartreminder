package com.example.prathamesh.smartreminder.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.model.GroupModel;
import com.example.prathamesh.smartreminder.model.MyFamilymodel;
import com.example.prathamesh.smartreminder.model.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class MyFamilyActivity extends AppCompatActivity {

    //View
    private EditText editTextName, editTextNumber;
    private Button buttonOk, btnNotification;
    public static String mobileNo;
    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String userId, userIdForUpdate;
    String name, number;
    String Username, groupName, groupName1;
    String num, num2;

    ArrayList<String> contactList, contactList1;
    ListView listView;
    ArrayList<MyFamilymodel> myFamilyGroupList;
    ArrayList<GroupModel> myFamilymodelList;
    String grp_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_family);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        userId = user.getUid();
        mFirestore = FirebaseFirestore.getInstance();
        listView = (ListView) findViewById(R.id.listViewMember);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        buttonOk = (Button) findViewById(R.id.buttonOk);
        btnNotification = (Button) findViewById(R.id.notification_btn);
        btnNotification.setEnabled(false);
        myFamilyGroupList = new ArrayList<>();
        mobileNo = getMobileNo();
        getDataFromFirebase();
        getContactsFromMyFamily();

//            getFamilyInfo();
        //if(myFamilymodelList!= null) {
        fillGroupList();
        //}


//        Log.d("Mobile No ",mobileNo);
//        Log.d("Group nME",grp_name);


        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = editTextName.getText().toString();
                number = editTextNumber.getText().toString();
                boolean shouldCancelSignUp = false;
                View focusView = null;

                if (name.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = editTextName;
                    editTextName.setError("Group Name is a required field");
                }
                if (number.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = editTextNumber;
                    editTextNumber.setError("Number is a required field");
                }
//                if (number.matches(mobileNo)) {
//                    shouldCancelSignUp = true;
//                    focusView = editTextNumber;
//                    editTextNumber.setError("Please enter another number");
//
//                }
                if (shouldCancelSignUp) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }

                if (number.matches(mobileNo)) {
                    shouldCancelSignUp = true;
                    focusView = editTextNumber;
                    Toast.makeText(MyFamilyActivity.this, "Please do not enter your mobile", Toast.LENGTH_SHORT).show();

                } else {


                    boolean reg_flag = false, grp_flag = false;

                    for (int i = 0; i < contactList.size(); i++) {
                        Log.d("REG NO ", contactList.get(i));
                        Log.d("REG NO1 ", number);

                        if (contactList.get(i).matches(number)) {
                            //sendDatatoFirebase();
                            reg_flag = false;
                            contactList.clear();

                        } else {

                            reg_flag = true;
                        }
                    }
                    for (int i = 0; i < contactList1.size(); i++) {
                        Log.d("SizeList", contactList1.size() + "");
                        if (contactList1.get(i).matches(mobileNo)) {
                            //sendDatatoFirebase();
                            //Snackbar.make(MyFamilyActivity.this.findViewById(android.R.id.content), "Mobile No Does not exist", Snackbar.LENGTH_SHORT).show();
                            grp_flag = true;
                        }
                        if (contactList1.get(i).matches(number)) {
                            grp_flag = true;
                        }
                    }
                    if (reg_flag || grp_flag) {
                        Snackbar.make(MyFamilyActivity.this.findViewById(android.R.id.content), "Mobile No Does not exist", Snackbar.LENGTH_SHORT).show();
                       // Toast.makeText(MyFamilyActivity.this, "Ikde aahe", Toast.LENGTH_SHORT).show();
                    } else {
                        sendDatatoFirebase();
                       // Toast.makeText(MyFamilyActivity.this, "Zaala", Toast.LENGTH_SHORT).show();

                        AlertDialog alertDialog = new AlertDialog.Builder(MyFamilyActivity.this)
                                .setCancelable(false)
                                .setTitle("Request")
                                .setMessage("Request is send to the group member")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //((AppCompatActivity) context).finish();
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                .create();

                        alertDialog.show();
                    }

                }
            }
        });


        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MyFamilyActivity.this);
                LayoutInflater inflater = LayoutInflater.from(MyFamilyActivity.this);
                View mv = inflater.inflate(R.layout.custom_alert_my_family, null);
                TextView textView = (TextView) mv.findViewById(R.id.textView_notification);
                Button yes = (Button) mv.findViewById(R.id.btn_yes);
                Button no = (Button) mv.findViewById(R.id.btn_no);
                final String userInfo = "You have been added in " + grp_name;
                textView.setText(userInfo);
                builder.setView(mv);
                final AlertDialog dialog = builder.create();
                dialog.show();

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Update the group table with yes where verified column is there

                        final Map<String, Object> flag = new HashMap<>();
                        flag.put("flag", "1");
//                         mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY)
//                                 .document(userId).set(flag, SetOptions.merge());

                        mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY)
                                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                                    if (grp_name.matches(documentSnapshot.getData().get("name").toString())) {
                                        mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY).document(userIdForUpdate)
                                                .set(flag, SetOptions.merge());
                                    }
                                }
                            }
                        });

                        AlertDialog alertDialog = new AlertDialog.Builder(MyFamilyActivity.this)
                                .setCancelable(false)
                                .setTitle("Success")
                                .setMessage("You are a member of the group...!!!")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //((AppCompatActivity) context).finish();
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                .create();

                        alertDialog.show();

                        Log.d("Here", "Here");
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final String u_name = myFamilymodelList.get(i).getUsername();
                mFirestore.collection(UserInfoModel.FIRESTORE_COLLECTION_USERINFO)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        if (u_name.matches(document.getData().get("fullName").toString())) {

                                            String u_id = document.getData().get("userId").toString();
                                            Log.d("U", u_name + "  " + u_id);
                                            HomeActivity.USER_ID = u_id;
                                            HomeActivity.OTHER_FLAG = "other_flag";
                                            Intent intent = new Intent(MyFamilyActivity.this, HomeActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            }
                        });

            }
        });
    }

    public void getDataFromFirebase() {
        mFirestore.collection(UserInfoModel.FIRESTORE_COLLECTION_USERINFO)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            contactList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                num = (String) document.getData().get("mobileNo");
                                contactList.add(num);
                            }
                        }
                    }
                });
    }

    public void getContactsFromMyFamily() {
        mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            contactList1 = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                num2 = (String) document.getData().get("number");
                                contactList1.add(num2);
                            }
                        }
                    }
                });
    }

    public void sendDatatoFirebase() {
        MyFamilymodel myFamilymodel = new MyFamilymodel(name, number, "0", userId);
        mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY)
                .document(userId).set(myFamilymodel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                } else {

                }
            }
        });
    }


    public String getMobileNo() {
        mFirestore.collection(UserInfoModel.FIRESTORE_COLLECTION_USERINFO)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (userId.matches(document.getData().get("userId").toString())) {
                                    mobileNo = (String) document.getData().get("mobileNo");
                                    Log.d("Mobile No1 ", mobileNo);

                                }
                            }

                            getFamilyInfo();
                        }
                    }
                });
        return mobileNo;
    }


    public class CustomAdapter extends ArrayAdapter<GroupModel> {
        List<GroupModel> myFamilymodelList;

        public CustomAdapter(Context context, int resource, List<GroupModel> myFamilymodelList) {
            super(context, resource, myFamilymodelList);
            this.myFamilymodelList = myFamilymodelList;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            TextView textViewName, textViewNumber;
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.custom_my_family, parent, false);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewNumber = (TextView) view.findViewById(R.id.textViewNumber);
            textViewName.setText(myFamilymodelList.get(position).getUsername());
            textViewNumber.setText(myFamilymodelList.get(position).getGroupName());

            return view;
        }
    }

    public void getFamilyInfo() {
        mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {


                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (mobileNo.matches(document.getData().get("number").toString())) {
                                    MyFamilymodel myFamilymodel = document.toObject(MyFamilymodel.class);
                                    myFamilyGroupList.add(myFamilymodel);

                                }
                            }

                            if (myFamilyGroupList != null) {
                                for (int i = 0; i < myFamilyGroupList.size(); i++) {

                                    if (myFamilyGroupList.get(i).getFlag().matches("0")) {
                                        btnNotification.setEnabled(true);
                                        grp_name = myFamilyGroupList.get(i).getName();
                                        userIdForUpdate = myFamilyGroupList.get(i).getUserId();

                                    }
                                }
                            }
                            Log.d("GrpSize", myFamilyGroupList.size() + "");
                        }

                    }
                });
    }

    public void fillGroupList() {

        mFirestore.collection(MyFamilymodel.FIRESTORE_COLLECTION_MYFAMILY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            myFamilymodelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (userId.matches(document.getData().get("userId").toString())) {
                                    Log.d("UID", userId);
                                    if (document.getData().get("flag").toString().matches("1")) {
                                        final String mobileNo = document.getData().get("number").toString();
                                        Log.d("Mobile", mobileNo);
                                        groupName = document.getData().get("name").toString();
                                        mFirestore.collection(UserInfoModel.FIRESTORE_COLLECTION_USERINFO)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                        if (task.isSuccessful()) {
                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                if (mobileNo.matches(document.getData().get("mobileNo").toString())) {

                                                                    Username = document.getData().get("fullName").toString();
                                                                    Log.d("User", Username);
                                                                    Log.d("GrpName", groupName);
                                                                    GroupModel groupModel = new GroupModel(Username, groupName);
                                                                    myFamilymodelList.add(groupModel);
                                                                }
                                                            }

                                                            CustomAdapter customAdapter = new CustomAdapter(MyFamilyActivity.this, android.R.layout.simple_list_item_1, myFamilymodelList);
                                                            listView.setAdapter(customAdapter);
                                                        }
                                                    }
                                                });


                                    }
                                } else if (mobileNo.matches(document.getData().get("number").toString())) {
                                    if (document.getData().get("flag").toString().matches("1")) {
                                        final String user_Id = document.getData().get("userId").toString();
                                        Log.d("UID1", user_Id);
                                        groupName1 = document.getData().get("name").toString();
                                        Log.d("GrpName1", groupName1);
                                        mFirestore.collection(UserInfoModel.FIRESTORE_COLLECTION_USERINFO)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                        if (task.isSuccessful()) {
                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                if (user_Id.matches(document.getData().get("userId").toString())) {

                                                                    Username = document.getData().get("fullName").toString();
                                                                    Log.d("User1", Username);

                                                                    GroupModel groupModel = new GroupModel(Username, groupName1);
                                                                    myFamilymodelList.add(groupModel);
                                                                }
                                                            }

                                                            CustomAdapter customAdapter = new CustomAdapter(MyFamilyActivity.this, android.R.layout.simple_list_item_1, myFamilymodelList);
                                                            listView.setAdapter(customAdapter);
                                                        }
                                                    }
                                                });


                                    }
                                }


                                //    Log.d("Group",groupName);

                            }

                        }
                        //CustomAdapter customAdapter = new CustomAdapter(MyFamilyActivity.this,android.R.layout.simple_list_item_1,myFamilymodelList);
                        //listView.setAdapter(customAdapter);
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
