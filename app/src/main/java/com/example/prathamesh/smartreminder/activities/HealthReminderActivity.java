package com.example.prathamesh.smartreminder.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.model.CommonReminderModel;
import com.example.prathamesh.smartreminder.model.HealthReminderModel;
import com.example.prathamesh.smartreminder.services.MyGeneralAlarm;
import com.example.prathamesh.smartreminder.services.MyHealthAlarm;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class HealthReminderActivity extends AppCompatActivity {

    //VIEWS
    private EditText editTextName,editTextDays;
    private CheckBox checkBoxMorning,checkBoxAfternoon,checkBoxEvening,checkBoxNight;
    private Button buttonOk;
    int count =0;

    //VARIABLES
    String name,days,morning,afternoon,evening,night,userId,date;
    int gYear ,gMonth,gDay;
    ArrayList<String> heathTime ;
    ArrayList<String> healthTimeList;
    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_reminder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if(HomeActivity.OTHER_FLAG!=null) {

            userId= HomeActivity.USER_ID;
        }
        else
        {
            userId = user.getUid();
        }
        mFirestore = FirebaseFirestore.getInstance();
        editTextName =(EditText)findViewById(R.id.editTextName);
        editTextDays = (EditText)findViewById(R.id.editTextDays);
        checkBoxMorning = (CheckBox)findViewById(R.id.checkboxMorning);
        checkBoxAfternoon = (CheckBox)findViewById(R.id.checkboxAfternoon);
        checkBoxEvening = (CheckBox)findViewById(R.id.checkboxEvening);
        checkBoxNight = (CheckBox) findViewById(R.id.checkboxNight);
        buttonOk = (Button) findViewById(R.id.buttonOk);
        heathTime = new ArrayList<>();
        healthTimeList = new ArrayList<>();
        healthTimeList.add("morning");
        healthTimeList.add("afternoon");
        healthTimeList.add("evening");
        healthTimeList.add("night");
        MyHealthAlarm.context = this;

        Date currentdate = new Date();  // to get the date
        SimpleDateFormat df = new SimpleDateFormat("dd/M/yyyy"); // getting date in this format
        date = df.format(currentdate.getTime());

        final Calendar calendar = Calendar.getInstance();
        gYear= calendar.get(Calendar.YEAR);
        Log.d("YYY", String.valueOf(gYear));
        gMonth = calendar.get(Calendar.MONTH)+1;
        Log.d("MMM", String.valueOf(gMonth));
        gDay= calendar.get(Calendar.DATE);
        Log.d("DDD", String.valueOf(gDay));

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = editTextName.getText().toString();
                days = editTextDays.getText().toString();


                boolean shouldCancelSignUp = false;
                View focusView = null;

                if (name.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = editTextName;
                    editTextName.setError("Reminder name is a required field");
                }
                if (days.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = editTextDays;
                    editTextDays.setError("Description is a required field");
                }
                if (shouldCancelSignUp) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }

                if(checkBoxMorning.isChecked())
                {
                  heathTime.add("9:30 AM");

                    Calendar calendar = Calendar.getInstance();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                9,
                                30,
                                0);


                    } else {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                9,
                                30,
                                0);
                    }
                    AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


                    Intent intent = new Intent(HealthReminderActivity.this, MyHealthAlarm.class);
                    final int i = (int) System.currentTimeMillis();
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(HealthReminderActivity.this, i, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() , AlarmManager.INTERVAL_DAY, pendingIntent);
                    Toast.makeText(HealthReminderActivity.this, "Alarm is set", Toast.LENGTH_SHORT).show();

                    intentArray.add(pendingIntent);
                }
                else
                {
                    heathTime.remove(healthTimeList.get(0));
                }
                if(checkBoxAfternoon.isChecked())
                {

                    heathTime.add("12:30 PM");
                    Calendar calendar = Calendar.getInstance();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                12,
                                30,
                                0);


                    } else {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                12,
                                30,
                                0);
                    }
                    AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


                    Intent intent = new Intent(HealthReminderActivity.this, MyHealthAlarm.class);
                    final int i = (int) System.currentTimeMillis();
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(HealthReminderActivity.this, i, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() , AlarmManager.INTERVAL_DAY, pendingIntent);
                    Toast.makeText(HealthReminderActivity.this, "Alarm is set", Toast.LENGTH_SHORT).show();

                    intentArray.add(pendingIntent);

                }
                else
                {

                    heathTime.remove(healthTimeList.get(1));
                }

                if(checkBoxEvening.isChecked())
                {
                    heathTime.add("16:30 PM");
                    Calendar calendar = Calendar.getInstance();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                16,
                                30,
                                0);


                    } else {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                16,
                                30,
                                0);
                    }
                    AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


                    Intent intent = new Intent(HealthReminderActivity.this, MyHealthAlarm.class);
                    final int i = (int) System.currentTimeMillis();
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(HealthReminderActivity.this, i, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() , AlarmManager.INTERVAL_DAY, pendingIntent);
                    Toast.makeText(HealthReminderActivity.this, "Alarm is set", Toast.LENGTH_SHORT).show();

                    intentArray.add(pendingIntent);

                }
                else
                {

                    heathTime.remove(healthTimeList.get(2));
                }
                if(checkBoxNight.isChecked())
                {

                    heathTime.add("21:30 PM");
                    Calendar calendar = Calendar.getInstance();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                21,
                                30,
                                0);


                    } else {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                21,
                                30,
                                0);
                    }
                    AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


                    Intent intent = new Intent(HealthReminderActivity.this, MyHealthAlarm.class);
                    final int i = (int) System.currentTimeMillis();
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(HealthReminderActivity.this, i, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() , AlarmManager.INTERVAL_DAY, pendingIntent);
                   // Toast.makeText(HealthReminderActivity.this, "Alarm is set", Toast.LENGTH_SHORT).show();

                    intentArray.add(pendingIntent);

                }
                else
                {

                    heathTime.remove(healthTimeList.get(3));
                }
                if(heathTime.size()==0)
                {
                    Toast.makeText(HealthReminderActivity.this,"Please select the Timing",Toast.LENGTH_SHORT).show();
                }
                else {
                sendDataToFirebase();
                sendCommonDataToFirebase();

                    final AlertDialog.Builder mBuild = new AlertDialog.Builder(HealthReminderActivity.this);
                    LayoutInflater inflater = (LayoutInflater) HealthReminderActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View mv = inflater.inflate(R.layout.progress, null);
                    final ProgressBar progressBar =(ProgressBar)mv.findViewById(R.id.progress);
                    final Button b2=(Button)mv.findViewById(R.id.b2);
                    final TextView t1=(TextView)mv.findViewById(R.id.t1);
                    b2.setVisibility(View.INVISIBLE);
                    mBuild.setView(mv);
                    mBuild.setCancelable(false);
                    final AlertDialog dialog = mBuild.create();
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setLayout(1100, 500);
                    final Handler handler= new Handler();
                    final Timer timer=new Timer();
                    final Runnable runnable=new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            t1.setVisibility(View.VISIBLE);
                            t1.setText("Reminder set Successfully");
                            b2.setVisibility(View.VISIBLE);
                            timer.cancel();
                            b2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    startActivity(new Intent(HealthReminderActivity.this,HomeActivity.class));
                                    finish();
                                }
                            });

                        }
                    };

                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(runnable);
                        }
                    },2000,500);

                }



            }
        });
    }
    public void sendDataToFirebase()
    {
        HealthReminderModel healthReminderModel = new HealthReminderModel(name, days,date, heathTime,userId);
        mFirestore.collection(HealthReminderModel.FIRESTORE_COLLECTION_HEALTH)
                .document().set(healthReminderModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                  //  Toast.makeText(HealthReminderActivity.this, "data added", Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(HealthReminderActivity.this, "data not added", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public void sendCommonDataToFirebase()
    {

        CommonReminderModel commonReminderModel = new CommonReminderModel(name,heathTime,userId);
        mFirestore.collection(CommonReminderModel.FIRESTORE_COLLECTION_COMMON)
                .document().set(commonReminderModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // Toast.makeText(GeneralReminderActivity.this, "data added", Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(GeneralReminderActivity.this, "data not added", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
