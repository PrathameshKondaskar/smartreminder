package com.example.prathamesh.smartreminder.activities;


import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.model.CommonReminderModel;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.services.MyGeneralAlarm;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class GeneralReminderActivity extends AppCompatActivity {
    //VIEW
    private Button buttonDate,buttonTime,buttonOk;
    private DatePickerDialog datePickerDialog;
    private EditText editTextName,editTextDescription;
    private Spinner spinnerDuration;
    //VARIABLES
    String date ,time,duration,name,description,userId;
    int gYear ,gMonth,gDay,gHour,gMinute;
    ArrayList<GeneralReminderModel> generalReminderModelList;

   public  static ArrayList<String> namelist =new ArrayList<>();
   public static ArrayList<String> tarikhlist =new ArrayList<>();
   public  static ArrayList<String> vellist =new ArrayList<>();
    ArrayList<String> timeList;


    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_reminder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if(HomeActivity.OTHER_FLAG!=null) {

            userId= HomeActivity.USER_ID;
        }
        else
        {
            userId = user.getUid();
        }
        MyGeneralAlarm.context = this;
        mFirestore = FirebaseFirestore.getInstance();
        timeList = new ArrayList<>();
        editTextName =(EditText)findViewById(R.id.editTextName);
        editTextDescription =(EditText)findViewById(R.id.editTextDescription);
        buttonDate = (Button) findViewById(R.id.pickDateGeneral);
        buttonTime = (Button) findViewById(R.id.pickTimeGeneral);
        buttonOk = (Button) findViewById(R.id.buttonOk);
        spinnerDuration = (Spinner)findViewById(R.id.spinnerDuration);

        String[] arraySpinner = new String[] {
                "Daily", "Weekly","Monthly"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDuration.setAdapter(adapter);
        spinnerDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                duration = spinnerDuration.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Date currentdate = new Date();  // to get the date
        SimpleDateFormat df = new SimpleDateFormat("dd/M/yyyy"); // getting date in this format
        String formattedDate = df.format(currentdate.getTime());
        date = formattedDate;
        buttonDate.setText(formattedDate);
        final Calendar calendar = Calendar.getInstance();
        gYear= calendar.get(Calendar.YEAR);
        Log.d("YYY", String.valueOf(gYear));
        gMonth = calendar.get(Calendar.MONTH)+1;
        Log.d("MMM", String.valueOf(gMonth));
        gDay= calendar.get(Calendar.DATE);
        Log.d("DDD", String.valueOf(gDay));
        gHour=calendar.get(Calendar.HOUR);
        Log.d("HHH", String.valueOf(gHour));
        gMinute=calendar.get(Calendar.MINUTE);
        Log.d("MIMI", String.valueOf(gMinute));
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm a");
        String currentTime1=dateFormat.format(currentTime);
        time= currentTime1;
        buttonTime.setText(currentTime1);
        timeList.add(currentTime1);
        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog = new DatePickerDialog(GeneralReminderActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                buttonDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                gYear = year;
                                gMonth=monthOfYear+1;
                                gDay=dayOfMonth;
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        buttonTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;

                    mTimePicker = new TimePickerDialog(GeneralReminderActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                            String AM_PM ;
                            if(selectedHour < 12) {
                                AM_PM = "AM";
                            } else {
                                AM_PM = "PM";
                            }



                            if (selectedHour < 10) {
                                buttonTime.setText("0" + selectedHour + ":" + selectedMinute + " " + AM_PM);
                            }
                             if (selectedMinute < 10) {
                                buttonTime.setText(selectedHour + ":0" + selectedMinute + " " + AM_PM);
                            }
                            if (selectedHour < 10 && selectedMinute < 10) {
                                buttonTime.setText("0" + selectedHour + ":0" + selectedMinute + " " + AM_PM);
                            }
                            if (selectedHour > 10 && selectedMinute > 10){
                                buttonTime.setText(selectedHour + ":" + selectedMinute + " " + AM_PM);
                            }
                            time = selectedHour + ":" + selectedMinute + " " + AM_PM;
                            timeList.remove(0);
                            timeList.add(time);
                            gHour = selectedHour;
                            gMinute = selectedMinute;
                        }
                    }, hour, minute, false);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();

//
            }
        });

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = editTextName.getText().toString();
                description = editTextDescription.getText().toString();

                boolean shouldCancelSignUp = false;
                View focusView = null;

                if (name.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = editTextName;
                    editTextName.setError("Reminder name is a required field");
                }
                if (description.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = editTextDescription;
                    editTextDescription.setError("Description is a required field");
                }
                if (buttonTime.getText().toString().matches("")) {
                    shouldCancelSignUp = true;
                    focusView = buttonTime;
                    buttonTime.setError("Select the Time");
                }


                if (buttonDate.getText().toString().matches("")) {
                    shouldCancelSignUp = true;
                    focusView = buttonDate;
                    buttonDate.setError("Select the Date");
                }
                if (shouldCancelSignUp) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }
                else {

                    sendDataToFirebase();
                    sendCommonDataToFirebase();


                    Calendar calendar = Calendar.getInstance();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                gHour,
                                gMinute,
                                0);

                        Log.d("Min", String.valueOf(gMinute));
                        Log.d("year", String.valueOf(gYear));
                        Log.d("mon", String.valueOf(gMonth-1));
                        Log.d("day", String.valueOf(gDay));

                    } else {
                        calendar.set(
                                gYear,
                                gMonth - 1,
                                gDay,
                                gHour,
                                gMinute,
                                0);
                    }
                    Log.d("Mili", String.valueOf(calendar.getTimeInMillis()));
                    setAlarm(calendar.getTimeInMillis());


                    final AlertDialog.Builder mBuild = new AlertDialog.Builder(GeneralReminderActivity.this);
                    LayoutInflater inflater = (LayoutInflater) GeneralReminderActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View mv = inflater.inflate(R.layout.progress, null);
                    final ProgressBar progressBar =(ProgressBar)mv.findViewById(R.id.progress);
                    final Button b2=(Button)mv.findViewById(R.id.b2);
                    final TextView t1=(TextView)mv.findViewById(R.id.t1);
                    b2.setVisibility(View.INVISIBLE);
                    mBuild.setView(mv);
                    mBuild.setCancelable(false);
                    final AlertDialog dialog = mBuild.create();
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setLayout(1100, 500);
                    final Handler handler= new Handler();
                    final Timer timer=new Timer();
                    final Runnable runnable=new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            t1.setVisibility(View.VISIBLE);
                            t1.setText("Reminder set Successfully");
                            b2.setVisibility(View.VISIBLE);
                            timer.cancel();
                            b2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    startActivity(new Intent(GeneralReminderActivity.this,HomeActivity.class));
                                    finish();
                                }
                            });

                        }
                    };

                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(runnable);
                        }
                    },2000,500);


                }

            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public  void setAlarm(long timeInMillis) {

        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(this, MyGeneralAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
       // Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);


    }

    public void sendDataToFirebase()
    {

        GeneralReminderModel generalReminderModel = new GeneralReminderModel(name,description,date,time,duration,userId);
        mFirestore.collection(GeneralReminderModel.FIRESTORE_COLLECTION_GENERAL)
                .document().set(generalReminderModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
               //     Toast.makeText(GeneralReminderActivity.this, "data added", Toast.LENGTH_LONG).show();
                } else {
                 //   Toast.makeText(GeneralReminderActivity.this, "data not added", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void sendCommonDataToFirebase()
    {

        CommonReminderModel commonReminderModel = new CommonReminderModel(name,timeList,userId);
        mFirestore.collection(CommonReminderModel.FIRESTORE_COLLECTION_COMMON)
                .document().set(commonReminderModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                   // Toast.makeText(GeneralReminderActivity.this, "data added", Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(GeneralReminderActivity.this, "data not added", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public void getDataFromFireBase()
    {
        mFirestore.collection(GeneralReminderModel.FIRESTORE_COLLECTION_GENERAL)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful())
                        {
                            Log.d("TAG", task.getResult().isEmpty() + " => ");
                            generalReminderModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if(userId.matches((String) document.getData().get("userId")))
                                {
                                    GeneralReminderModel generalReminderModel = document.toObject(GeneralReminderModel.class);
                                    generalReminderModelList.add(generalReminderModel);
                                    //Toast.makeText(GeneralReminderActivity.this, " data retrieved", Toast.LENGTH_SHORT).show();
                                }

                            }


                        }
                        else
                        {

                        }
                    }
                });


    }

}
