package com.example.prathamesh.smartreminder.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.prathamesh.smartreminder.R;
import com.example.prathamesh.smartreminder.fragments.RemindersFragment;
import com.example.prathamesh.smartreminder.model.GeneralReminderModel;
import com.example.prathamesh.smartreminder.services.MyGeneralAlarm;

import java.util.ArrayList;
import java.util.Calendar;


public class SetAlarmsActivity extends AppCompatActivity {

 public static ArrayList<GeneralReminderModel> generalReminderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarms);
            Log.d("Size1 ",generalReminderList.size()+"");
        if(generalReminderList != null)
        {

            Log.d("HERE","here");
            for(int i=0; i< generalReminderList.size();i++)
            {
                String date =  generalReminderList.get(i).getDate();
                String time =generalReminderList.get(i).getTime();
                String[] val = date.split("/");
                int day = Integer.parseInt(val[0]);
                int month = Integer.parseInt(val[1]);
                int year = Integer.parseInt(val[2]);
                Log.d("Year",year+"");
                Log.d("Month",month+"");
                Log.d("Day",day+"");
                String[] valtime = time.split(":");
                Log.d("Time",time+"");
                int hour = Integer.parseInt(valtime[0]);
                String[] valmin = valtime[1].split(" ");
                int min = Integer.parseInt( valmin[0]);

                Calendar calendar = Calendar.getInstance();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    calendar.set(
                            year,
                            month-1,
                            day,
                            hour,
                            min,
                            0);

                    Log.d("Mini", String.valueOf(min));
                    Log.d("year", String.valueOf(year));
                    Log.d("mon", String.valueOf(month));
                    Log.d("day", String.valueOf(day));

                } else {
                    calendar.set(
                            year,
                            month-1,
                            day,
                            hour,
                            min,
                            0);
                }
                Log.d("Mili", String.valueOf(calendar.getTimeInMillis()));
                setAlarm(calendar.getTimeInMillis());
            Intent intent = new Intent(SetAlarmsActivity.this,HomeActivity.class);
            RemindersFragment.XXX = "xxx";

//                Fragment fragment = new RemindersFragment();
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ft.replace(R.id.content_home,fragment);
//                ft.commit();


            }
        }




    }
    public  void setAlarm(long timeInMillis) {

        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(this, MyGeneralAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
         Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);


    }
}
