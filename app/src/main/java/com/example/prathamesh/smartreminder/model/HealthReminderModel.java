package com.example.prathamesh.smartreminder.model;

import java.util.List;

public class HealthReminderModel {

    public final static String FIRESTORE_COLLECTION_HEALTH = "healthReminders";

    String name,days,date;
    private List<String> healthtime;
    String userId;

    public HealthReminderModel() {
    }

    public HealthReminderModel(String name, String days, String date, List<String> healthtime, String userId) {
        this.name = name;
        this.days = days;
        this.date = date;
        this.healthtime = healthtime;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public List<String> getHealthtime() {
        return healthtime;
    }

    public void setHealthtime(List<String> healthtime) {
        this.healthtime = healthtime;
    }
}
